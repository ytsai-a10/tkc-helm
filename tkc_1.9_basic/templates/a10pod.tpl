{{/*
  #           - --watch-namespace=$(WATCH_NAMESPACE)
  #           - --ingress-class=a10-ext # Specifies a class of ingress. A10 Kubernetes Connector will only process Ingress resources with this value in the ingress class annotation. Can be used with --use-ingress-class-only. Default 'a10'.  Ingress class annotation will typically look like this kubernetes.io/ingress.class: "a10-ext"
  #           - --use-ingress-class-only=true #If true, A10 Kubernetes Connector will only process Ingress resources that has the ingress
  #           - -v=3
Create at container part
*/}}

{{- define "a10-connector.a10pod" -}}
- name: {{ template "a10-connector.fullname" . }}
  image: {{ template "a10-connector.image" . }}
  imagePullPolicy: {{ .Values.imageCredentials.pullPolicy }}
  env:
  - name: POD_NAMESPACE
    valueFrom:
        fieldRef:
          fieldPath: metadata.namespace
  - name: CONTROLLER_URL
    value: {{ .Values.lbUrl | quote }}
  - name: ACOS_USERNAME_PASSWORD_SECRETNAME
    value: {{ template "a10-connector.fullname" . }}-secret
  - name: PARTITION 
    value: {{ .Values.partition | quote }}
  args:
  - --include-all-nodes={{ .Values.args.allNodes }}
  - --safe-acos-delete={{ .Values.args.safeDelete }}

{{- end }}
