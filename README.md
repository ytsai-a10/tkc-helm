# tkc-helm

Helm Charts for Thunder Kubernetes Controller (i.e. TKC)

This Project maintain TKC Helm Charts, and the Helm Charts repo URL is in tkc-doc project.
For example:
```shell
helm repo add <name-you-want> https://a10networks.gitlab.io/tkc-doc/tkc_1.9_basic
```

## Helm Charts

```
| Directory            | TKC version   | Description                                 |
| tkc_1.9_basic        | v1.9+         | Basic deployment for TKC v1.9               |
```
